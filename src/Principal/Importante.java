
package Principal;
import procedimientoAgenda.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wendy
 */

public abstract class Importante {
    
    public static void main(String[] args) {
        try {
            BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
            String texto="";
            char opc='1';
            Agenda agendaElectronica= new Agenda();
            while ((opc=='1') || (opc=='2') || (opc=='3') || (opc=='4') )
            {
                System.out.println("╔═══════════════╗");
                System.out.println( "   MI AGENDA PERSONAL      " );
                System.out.println("╚═══════════════╝");
                System.out.println("   1-Agregar contacto");
                System.out.println("   2-Buscar  contacto");
                System.out.println("   3-Modificar contacto");
                System.out.println("   4-Eliminar contacto");
                System.out.println("   5-Ver numero de caracteres ");
                System.out.println("   0-Salir");
                System.out.println("╚═══════════════╝");
                texto=leer.readLine();
                opc= texto.charAt(0);
                System.out.println("Opción: ");
                System.out.println("......................");
                
                switch(opc){
                   case '1':
                        String nombre;
                        String apellido;
                        String direccion;
                        String genero;
                        String telefono;
                        boolean validar;
                        System.out.println("Por favor introduzca el nombre:");
                        nombre=leer.readLine();
                        System.out.println("Por favor introduzca el apellido:");
                        apellido=leer.readLine();
                        System.out.println("Por favor introduzca la direccion");
                        direccion=leer.readLine();
                        System.out.println("Por favor introduzca el genero :");
                        genero=leer.readLine();
                        System.out.println("Por favor introduzca el teléfono(numero):");
                        telefono=leer.readLine();
                        validar=esNumerica(telefono);
                        if (validar){
                             int telefono_entero= Integer.parseInt(telefono);
                             agendaElectronica.Consultar(nombre,apellido, direccion, genero, telefono_entero);
                             agendaElectronica.Registrar(nombre,apellido, direccion, genero, telefono_entero);
                        }
                        else{
                             System.out.println("No es un número, formato de telefono incorrecto.");}
                        
                       
                        break;
                        
                   case '2':
                        
                        System.out.println("Nombre a buscar:");
                        nombre=leer.readLine().toUpperCase();
                        agendaElectronica.Buscar(nombre);
                        break;
                    case '3':
                        agendaElectronica.Modificar();
                        break;
                    case '4':
                        agendaElectronica.Eliminar();
                        break;
                    case '5':
                        agendaElectronica.canCaracteres();
                        break;
                    case '0':
                        System.out.println("Ha salido del programa");
                        break;
                  default:
                        System.out.println("Opción incorrecta ...");
                        opc='1';
                     
                }
                
            }    
            } catch (IOException ex) {
            Logger.getLogger(Importante.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static boolean esNumerica(String str)
{
    for (char c : str.toCharArray())
    {
        if (!Character.isDigit(c)) return false;
    }
    return true;
}
}