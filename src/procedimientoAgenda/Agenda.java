
package procedimientoAgenda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wendy
 */
public class Agenda extends Persona {

    Persona [] ListaContactos = new Persona[100];
    private int Contactos = 0; // Contador de objetos creados. Variable muy importante.
 
    public void Consultar(String nombre,String apellido,String direccion,String genero, int telefono) {
        for (int i = 0; i < this.Contactos; i++) {
 
            if (nombre.equals(this.ListaContactos[i].getNombre())) {
                System.out.println("Ya existe un contacto con ese nombre");
            }
        }
 
    }
    
        public void Registrar (String nombre,String apellido,String direccion,String genero, int telefono){
        if (Contactos < 100) {
            this.ListaContactos[Contactos] = new Persona();
            this.ListaContactos[Contactos].set_nombre(nombre);
            this.ListaContactos[Contactos].set_apellido(apellido);
            this.ListaContactos[Contactos].set_direccion(direccion);
            this.ListaContactos[Contactos].set_genero(genero);
            this.ListaContactos[Contactos].set_telefono(telefono);
            this.Contactos++;
            Ordenar();
        } else {
            System.out.println("La agenda está llena");
        }
 
    } 
    public void Buscar(String nombre) {
        boolean encontrado = false;
 
        for (int i = 0; i < Contactos; i++) {
            if (nombre.equals(this.ListaContactos[i].getNombre())) {
                System.out.println( " NOMBRE = " + this.ListaContactos[i].getNombre() + "  - APELLIDO = " + this.ListaContactos[i].getApellido()  + " - DIRECCION = " + this.ListaContactos[i].getDireccion() + " - GENERO = " +this.ListaContactos[i].getGenero() + " - TELEFONO = " + this.ListaContactos[i].getTelefono());
                encontrado = true;
            }
        }
        if (!encontrado) {
            System.out.println("Contacto inexistente");
        }
    }
 
     public void Ordenar() {
        //Este método ordenará el array de contacos por el nombre mediante el Método Burbuja
        int N = this.Contactos;
        String apellido1;
        String apellido2;
        //Optimizo para cuando tenga más de dos elementos al menos.
        if (Contactos >= 2) {
            for (int i = 1; i <= N - 1; i++) {
                for (int j = 1; j <= N - i; j++) {
                    apellido1 = this.ListaContactos[j - 1].getApellido();
                    apellido2 = this.ListaContactos[j].getApellido();
                    if (apellido1.charAt(0) > apellido2.charAt(0)) {
                        Persona tmp = this.ListaContactos[j - 1];
                        this.ListaContactos[j - 1] = this.ListaContactos[j];
                        this.ListaContactos[j] = tmp;
                    }
                }
            }
        }
    }
 
    public void Eliminar() {
        try {
            boolean encontrado = false;
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Nombre de contacto a eliminar:");
            String eliminar = teclado.readLine().toUpperCase();
            if (Contactos == 0) {
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < Contactos; i++) {
               // traemos los datos necesarios
                    if (eliminar.equals(this.ListaContactos[i].getNombre())) {
                        System.out.println(i + 1 + ". " + this.ListaContactos[i].getNombre() + "-" + "Telefono:" + this.ListaContactos[i].getTelefono()+ "-" + "direccion:" +this.ListaContactos[i].getDireccion()+ "-" + "correo:" + this.ListaContactos[i].getGenero());
                        encontrado = true;
                    }
                }
                if (encontrado) {
                    System.out.println("¿Qué contacto quieres eliminar, introduce el número asociado?");
                    int eliminar_numero = Integer.parseInt(teclado.readLine());
                    eliminar_numero--;
                    System.out.println("¿Estas seguro (S/N)?");
                    String respuesta;
                    respuesta = teclado.readLine();
                    respuesta = respuesta.toUpperCase();
                    if (respuesta.equals("S")) {
                        Persona[] temporal = new Persona[100];
                        int ii = 0;
                        boolean encontrado2=false;
                        for (int i = 0; i < this.Contactos; i++) {
 
                            if (i != eliminar_numero) {
                                // Creo el objeto temporal para el borrado
                                if (!encontrado2)
                                {
                                  temporal[ii] = this.ListaContactos[ii];
                                  ii++;
                                }
                                else
                                {
                                    if (ii<this.Contactos)
                                    { temporal[ii] = this.ListaContactos[ii+1];
                                     ii++;
                                    }
                                }
 
                            } else {
                                temporal[ii] = this.ListaContactos[ii + 1];
                                ii++;
                                encontrado2=true;
 
                            }
                        }
                        this.Contactos--;
                        System.out.println("Contacto eliminado correctamente");
                        for (int j = 0; j < this.Contactos; j++) {
                            this.ListaContactos[j] = temporal[j];
 
                        }
 
                    }
 
                } else {
                    System.out.println("Lo siento, No se ha encontrado el nombre");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  public void Modificar() {
        try {
            boolean encontrado = false;
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Nombre de contacto a modificar:");
            String eliminar = teclado.readLine().toUpperCase();
            if (Contactos == 0) {
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < this.Contactos; i++) {
 
                    if (eliminar.equals(this.ListaContactos[i].getNombre())) {
                        System.out.println(i + 1 + " NOMBRE = " + this.ListaContactos[i].getNombre() + " -  APELLIDO =   " + this.ListaContactos[i].getApellido()  + " - DIRECCIÓN = " + this.ListaContactos[i].getDireccion()+ " - GÉNERO = " + this.ListaContactos[i].getGenero()  + " -  TELEFONO = " + this.ListaContactos[i].getTelefono());
                        encontrado = true;
                    }
                }
                if (encontrado) {
                    System.out.println("¿Qué contacto quieres modificar?, introduce el número:");
                    int modificar_numero = Integer.parseInt(teclado.readLine());
 
                    System.out.println("Introduce nombre:");
                    String nombre_nuevo = teclado.readLine();
                    System.out.println("Introduce apellido:");
                    String apellido_nuevo = teclado.readLine();
                    System.out.println("Introduce direccion:");
                    String direccion_nuevo = teclado.readLine();
                    System.out.println("Introduce genero");
                    String genero_nuevo = teclado.readLine();
                    System.out.println("Introduce teléfono, formato numerico:");
                    int telefono_nuevo = Integer.parseInt(teclado.readLine());
                    
 
                    this.ListaContactos[modificar_numero - 1].set_nombre(nombre_nuevo);
                    this.ListaContactos[modificar_numero - 1].set_apellido(apellido_nuevo);
                    this.ListaContactos[modificar_numero - 1].set_direccion(direccion_nuevo);
                    this.ListaContactos[modificar_numero - 1].set_genero(genero_nuevo);
                    this.ListaContactos[modificar_numero - 1].set_telefono(telefono_nuevo);
                    
                    Ordenar();
                } else {
                    System.out.println("No hay contactos con ese nombre");
                }
 
            }
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void canCaracteres (){
        for (int w=0;w<Contactos; w++){
            System.out.println( " NOMBRE = " + this.ListaContactos[w].getNombre().length() + " -  APELLIDO =   " + this.ListaContactos[w].getApellido().length()  + " - DIRECCIÓN = " + this.ListaContactos[w].getDireccion().length() + " - GÉNERO = " + this.ListaContactos[w].getGenero().length()  + " -  TELEFONO = " + this.ListaContactos[w].getTelefono());
        }
    }
 
}
                  
     

