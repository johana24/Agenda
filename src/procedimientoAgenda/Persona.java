
package procedimientoAgenda;

/**
 *
 * @author Wendy
 */
public class Persona  {
    private String nombre;
    private String apellido;
    private String direccion;
    private String genero;
    private int telefono;
    
    
  public Persona()
    {
    this.nombre=null;
    this.apellido=null;
    this.direccion=null;
    this.genero=null;
    this.telefono=0;
    
    }
    public Persona(String nombre, String apellido,String direccion, String genero,  int telefono) {
        this.nombre = nombre;
        this.apellido=apellido;
        this.telefono = telefono;
        this.direccion= direccion;
        this.genero=genero;
    }
    public void set_nombre(String nomb){        
        this.nombre=nomb.toUpperCase();
    }
    public void set_apellido(String apellido){
        this.apellido= apellido.toUpperCase();
    } 
    public void set_telefono(int telf){
        this.telefono=telf;
    }
    
    public String getNombre() {
        return this.nombre;
    }
    public String getApellido() {
        return this.apellido;
    }
 
    public int getTelefono() {
        return telefono;
    }    
    
     public void set_direccion(String direccion){        
        this.direccion=direccion.toUpperCase();
    }
    public void set_genero(String genero){
        this.genero= genero.toUpperCase();
    } 
    public String getDireccion() {
        return this.direccion;
    }
    public String getGenero() {
        return this.genero;
    }
 
  
}